﻿using Microsoft.EntityFrameworkCore;

namespace SpectruMeter.Apis.DataApi.DAL.Models
{
    [Keyless]
    public class AggregatedResult
    {
        public DateTime DefinedInteval { get; set; }

        /// <summary>
        /// Positive active instantaneous power (A+) [kW]
        /// </summary>
        public decimal PositiveActiveInstantaneousPower { get; set; }

        /// <summary>
        /// Positive active energy (A+) total [kWh]
        /// </summary>
        public decimal PositiveActiveEnergy { get; set; }

        /// <summary>
        /// Negative active instantaneous power (A-) [kW]
        /// </summary>
        public decimal NegativeActiveInstantaneousPower { get; set; }

        /// <summary>
        /// Negative active energy (A+) total [kWh]
        /// </summary>
        public decimal NegativeActiveEnergy { get; set; }

        /// <summary>
        /// Positive reactive energy (Q+) total [kvarh]
        /// </summary>
        public decimal PositiveReactiveEnergy { get; set; }

        /// <summary>
        /// Negative reactive energy (Q-) total [kvarh]
        /// </summary>
        public decimal NegativeReactiveEnergy { get; set; }

        /// <summary>
        /// Sum active instantaneous power(A+ - A-) [kW]
        /// </summary>
        public decimal SumActiveInstantaneousPower { get; set; }

        /// <summary>
        /// Instantaneous current(I) in phase L1[A]
        /// </summary>
        public decimal InstantaneousCurrentInPhaseL1 { get; set; }

        /// <summary>
        /// Instantaneous voltage (U) in phase L1 [V]
        /// </summary>
        public decimal InstantaneousVoltageInPhaseL1 { get; set; }

        /// <summary>
        /// Instantaneous current(I) in phase L2[A]
        /// </summary>
        public decimal InstantaneousCurrentInPhaseL2 { get; set; }

        /// <summary>
        /// Instantaneous voltage (U) in phase L2 [V]
        /// </summary>
        public decimal InstantaneousVoltageInPhaseL2 { get; set; }

        /// <summary>
        /// Instantaneous current(I) in phase L3[A]
        /// </summary>
        public decimal InstantaneousCurrentInPhaseL3 { get; set; }

        /// <summary>
        /// Instantaneous voltage (U) in phase L3 [V]
        /// </summary>
        public decimal InstantaneousVoltageInPhaseL3 { get; set; }
    }
}
