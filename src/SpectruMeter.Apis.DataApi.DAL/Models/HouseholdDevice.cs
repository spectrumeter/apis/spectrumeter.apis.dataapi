﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SpectruMeter.Apis.DataApi.DAL.Models
{
    public class HouseholdDevice
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string? DeviceId { get; set; }
        public Guid HouseholdId { get; set; }
        public DateTime InsertedAt { get; set; }
    }
}
