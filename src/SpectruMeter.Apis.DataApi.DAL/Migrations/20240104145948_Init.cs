﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SpectruMeter.Apis.DataApi.DAL.Migrations
{
    /// <inheritdoc />
    public partial class Init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HouseholdDevices",
                columns: table =>
                    new
                    {
                        Id = table.Column<Guid>(type: "uuid", nullable: false),
                        DeviceId = table.Column<string>(type: "text", nullable: true),
                        HouseholdId = table.Column<Guid>(type: "uuid", nullable: false)
                    },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HouseholdDevices", x => x.Id);
                }
            );

            migrationBuilder.CreateTable(
                name: "MeasurementDatas",
                columns: table =>
                    new
                    {
                        DeviceId = table.Column<string>(type: "text", nullable: false),
                        HouseholdId = table.Column<Guid>(type: "uuid", nullable: false),
                        MeasurementTime = table.Column<DateTimeOffset>(
                            type: "timestamp with time zone",
                            nullable: false
                        ),
                        PositiveActiveInstantaneousPower = table.Column<decimal>(
                            type: "numeric",
                            nullable: false
                        ),
                        PositiveActiveEnergy = table.Column<decimal>(
                            type: "numeric",
                            nullable: false
                        ),
                        NegativeActiveInstantaneousPower = table.Column<decimal>(
                            type: "numeric",
                            nullable: false
                        ),
                        NegativeActiveEnergy = table.Column<decimal>(
                            type: "numeric",
                            nullable: false
                        ),
                        PositiveReactiveEnergy = table.Column<decimal>(
                            type: "numeric",
                            nullable: false
                        ),
                        NegativeReactiveEnergy = table.Column<decimal>(
                            type: "numeric",
                            nullable: false
                        ),
                        SumActiveInstantaneousPower = table.Column<decimal>(
                            type: "numeric",
                            nullable: false
                        ),
                        InstantaneousCurrentInPhaseL1 = table.Column<decimal>(
                            type: "numeric",
                            nullable: false
                        ),
                        InstantaneousVoltageInPhaseL1 = table.Column<decimal>(
                            type: "numeric",
                            nullable: false
                        ),
                        InstantaneousCurrentInPhaseL2 = table.Column<decimal>(
                            type: "numeric",
                            nullable: false
                        ),
                        InstantaneousVoltageInPhaseL2 = table.Column<decimal>(
                            type: "numeric",
                            nullable: false
                        ),
                        InstantaneousCurrentInPhaseL3 = table.Column<decimal>(
                            type: "numeric",
                            nullable: false
                        ),
                        InstantaneousVoltageInPhaseL3 = table.Column<decimal>(
                            type: "numeric",
                            nullable: false
                        )
                    },
                constraints: table => { }
            );

            migrationBuilder.CreateIndex(
                name: "IX_HouseholdDevices_DeviceId",
                table: "HouseholdDevices",
                column: "DeviceId"
            );

            migrationBuilder.Sql(
                "SELECT create_hypertable( '\"MeasurementDatas\"', 'MeasurementTime');\n"
                    + "CREATE INDEX ix_symbol_deviceId ON \"MeasurementDatas\" (\"HouseholdId\", \"MeasurementTime\" DESC)"
            );
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(name: "HouseholdDevices");

            migrationBuilder.DropTable(name: "MeasurementDatas");
        }
    }
}
