﻿using SpectruMeter.Apis.DataApi.DAL.Models;

namespace SpectruMeter.Apis.DataApi.DAL.Repositories.Interfaces
{
    public interface IMeasurementDataRepository
    {
        Task<MeasurementData> InsertAsync(
            MeasurementData measurementData,
            CancellationToken cancellationToken = default
        );

        Task InsertHousehouldDevice(
            HouseholdDevice householdDevice,
            CancellationToken cancellationToken = default
        );

        Task<HouseholdDevice> GetHouseholdDeviceAsync(
            string deviceId,
            CancellationToken cancellationToken = default
        );

        Task<List<AggregatedResult>> GetMeasurementDataAsync(
            decimal interval,
            string intervalType,
            DateTime fromDate,
            DateTime toDate,
            List<Guid> households,
            CancellationToken cancellationToken = default
        );

        Task DeleteAsync(Guid householdId, CancellationToken cancellationToken = default);

        Task UpdateHouseholdAsync(
            Guid householdId,
            string deviceId,
            CancellationToken cancellationToken = default
        );
    }
}
