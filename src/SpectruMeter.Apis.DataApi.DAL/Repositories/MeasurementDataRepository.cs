﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SpectruMeter.Apis.DataApi.DAL.Models;
using SpectruMeter.Apis.DataApi.DAL.Repositories.Interfaces;
using System.Runtime.CompilerServices;

namespace SpectruMeter.Apis.DataApi.DAL.Repositories
{
    public class MeasurementDataRepository : IMeasurementDataRepository
    {
        private readonly ILogger<MeasurementDataRepository> _logger;
        private readonly DatabaseContext _databaseContext;

        public MeasurementDataRepository(
            ILogger<MeasurementDataRepository> logger,
            DatabaseContext databaseContext
        )
        {
            _logger = logger;
            _databaseContext = databaseContext;
        }

        public async Task DeleteAsync(
            Guid householdId,
            CancellationToken cancellationToken = default
        )
        {
            var sql = FormattableStringFactory.Create(
                "DELETE FROM \"MeasurementDatas\" WHERE \"HouseholdId\" = '{0}'",
                householdId
            );

            await _databaseContext.Database.ExecuteSqlRawAsync(sql.ToString(), cancellationToken);
        }

        public async Task<HouseholdDevice> GetHouseholdDeviceAsync(
            string deviceId,
            CancellationToken cancellationToken = default
        )
        {
            var householdDevice = await _databaseContext.HouseholdDevices
                .OrderByDescending(o => o.InsertedAt)
                .LastOrDefaultAsync(o => o.DeviceId == deviceId, cancellationToken);

            return householdDevice;
        }

        public async Task<List<AggregatedResult>> GetMeasurementDataAsync(
            decimal interval,
            string intervalType,
            DateTime fromDate,
            DateTime toDate,
            List<Guid> households,
            CancellationToken cancellationToken = default
        )
        {
            var sql = FormattableStringFactory.Create(
                "SELECT "
                    + "time_bucket('{0} {1}', \"MeasurementTime\") AS definedInteval, "
                    + "avg(\"PositiveActiveInstantaneousPower\") as \"PositiveActiveInstantaneousPower\","
                    + "avg(\"PositiveActiveEnergy\") as \"PositiveActiveEnergy\","
                    + "avg(\"NegativeActiveInstantaneousPower\") as \"NegativeActiveInstantaneousPower\","
                    + "avg(\"NegativeActiveEnergy\") as \"NegativeActiveEnergy\","
                    + "avg(\"PositiveReactiveEnergy\") as \"PositiveReactiveEnergy\","
                    + "avg(\"NegativeReactiveEnergy\") as \"NegativeReactiveEnergy\","
                    + "avg(\"SumActiveInstantaneousPower\") as \"SumActiveInstantaneousPower\","
                    + "avg(\"InstantaneousCurrentInPhaseL1\") as \"InstantaneousCurrentInPhaseL1\","
                    + "avg(\"InstantaneousVoltageInPhaseL1\") as \"InstantaneousVoltageInPhaseL1\","
                    + "avg(\"InstantaneousCurrentInPhaseL2\") as \"InstantaneousCurrentInPhaseL2\","
                    + "avg(\"InstantaneousVoltageInPhaseL2\") as \"InstantaneousVoltageInPhaseL2\","
                    + "avg(\"InstantaneousCurrentInPhaseL3\") as \"InstantaneousCurrentInPhaseL3\","
                    + "avg(\"InstantaneousVoltageInPhaseL3\") as \"InstantaneousVoltageInPhaseL3\""
                    + " FROM \"MeasurementDatas\" WHERE "
                    + "\"HouseholdId\" IN ('{2}') AND \"MeasurementTime\" >= '{3}' AND \"MeasurementTime\" <= '{4}' "
                    + "GROUP BY definedInteval",
                interval,
                intervalType,
                string.Join("','", households),
                fromDate.ToString("yyyy-MM-dd hh:mm:ss"),
                toDate.ToString("yyyy-MM-dd hh:mm:ss")
            );

            var result = _databaseContext.AggregatedResults.FromSqlRaw(sql.ToString()).ToList();

            return result;
        }

        public async Task<MeasurementData> InsertAsync(
            MeasurementData measurementData,
            CancellationToken cancellationToken = default
        )
        {
            var sql = FormattableStringFactory.Create(
                "INSERT INTO \"MeasurementDatas\" (\"DeviceId\", \"HouseholdId\", \"MeasurementTime\", \"PositiveActiveInstantaneousPower\", \"PositiveActiveEnergy\", \"NegativeActiveInstantaneousPower\", "
                    + "\"NegativeActiveEnergy\", \"PositiveReactiveEnergy\", \"NegativeReactiveEnergy\", \"SumActiveInstantaneousPower\", \"InstantaneousCurrentInPhaseL1\", \"InstantaneousVoltageInPhaseL1\","
                    + "\"InstantaneousCurrentInPhaseL2\", \"InstantaneousVoltageInPhaseL2\", \"InstantaneousCurrentInPhaseL3\", \"InstantaneousVoltageInPhaseL3\") VALUES "
                    + "('{0}', '{1}', '{2}', {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15})",
                measurementData.DeviceId,
                measurementData.HouseholdId,
                measurementData.MeasurementTime.ToString("yyyy-MM-dd hh:mm:ss"),
                measurementData.PositiveActiveInstantaneousPower,
                measurementData.PositiveActiveEnergy,
                measurementData.NegativeActiveInstantaneousPower,
                measurementData.NegativeActiveEnergy,
                measurementData.PositiveReactiveEnergy,
                measurementData.NegativeReactiveEnergy,
                measurementData.SumActiveInstantaneousPower,
                measurementData.InstantaneousCurrentInPhaseL1,
                measurementData.InstantaneousVoltageInPhaseL1,
                measurementData.InstantaneousCurrentInPhaseL2,
                measurementData.InstantaneousVoltageInPhaseL2,
                measurementData.InstantaneousCurrentInPhaseL3,
                measurementData.InstantaneousVoltageInPhaseL3
            );

            await _databaseContext.Database.ExecuteSqlRawAsync(sql.ToString(), cancellationToken);

            return measurementData;
        }

        public async Task InsertHousehouldDevice(
            HouseholdDevice householdDevice,
            CancellationToken cancellationToken = default
        )
        {
            await _databaseContext.HouseholdDevices.AddAsync(householdDevice, cancellationToken);
            await _databaseContext.SaveChangesAsync(cancellationToken);
        }

        public async Task UpdateHouseholdAsync(
            Guid householdId,
            string deviceId,
            CancellationToken cancellationToken = default
        )
        {
            var householdDb = await _databaseContext.HouseholdDevices.FirstOrDefaultAsync(
                o => o.HouseholdId == householdId
            );
            if (householdDb != null)
            {
                householdDb.DeviceId = deviceId;
                await _databaseContext.SaveChangesAsync(cancellationToken);
            }
            else
            {
                await _databaseContext.HouseholdDevices.AddAsync(
                    new HouseholdDevice
                    {
                        DeviceId = deviceId,
                        HouseholdId = householdId,
                        InsertedAt = DateTime.Now
                    },
                    cancellationToken
                );
                await _databaseContext.SaveChangesAsync(cancellationToken);
            }
            //var sql = FormattableStringFactory.Create(
            //    "UPDATE \"HouseholdDevices\" SET \"DeviceId\" = '{0}' WHERE \"HouseholdId\" = '{1}'",
            //    deviceId,
            //    householdId
            //);

            //await _databaseContext.Database.ExecuteSqlRawAsync(sql.ToString(), cancellationToken);
        }
    }
}
