﻿using Microsoft.EntityFrameworkCore;
using SpectruMeter.Apis.DataApi.DAL.Models;

namespace SpectruMeter.Apis.DataApi.DAL
{
    public class DatabaseContext : DbContext
    {
        private readonly DbContextOptions<DatabaseContext> _options;
        public DbSet<MeasurementData> MeasurementDatas { get; set; }
        public DbSet<HouseholdDevice> HouseholdDevices { get; set; }
        public DbSet<AggregatedResult> AggregatedResults { get; set; }

        public DatabaseContext() { }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {
            _options = options;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql(
                    "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;Trust Server Certificate=False;Application Intent=ReadWrite;Multi Subnet Failover=False"
                );
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<HouseholdDevice>().HasIndex(o => o.DeviceId);
        }
    }
}
