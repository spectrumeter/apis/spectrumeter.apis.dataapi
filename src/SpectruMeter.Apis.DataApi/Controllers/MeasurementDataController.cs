﻿using Mapster;
using Microsoft.AspNetCore.Mvc;
using SpectruMeter.Apis.DataApi.DAL.Repositories.Interfaces;
using SpectruMeter.Apis.DataApi.Models;

namespace SpectruMeter.Apis.DataApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MeasurementDataController : ControllerBase
    {
        private readonly ILogger<MeasurementDataController> _logger;
        private readonly IMeasurementDataRepository _measurementDataRepository;

        public MeasurementDataController(
            ILogger<MeasurementDataController> logger,
            IMeasurementDataRepository measurementDataRepository
        )
        {
            _logger = logger;
            _measurementDataRepository = measurementDataRepository;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<MeasurementDatasResponseDto>> Get(
            [FromBody] MeasurementDatasRequestDto request
        )
        {
            var data = await _measurementDataRepository.GetMeasurementDataAsync(
                request.Interval,
                request.IntervalType,
                request.FromDate,
                request.ToDate,
                request.HouseholdIds
            );
            return StatusCode(
                StatusCodes.Status200OK,
                new MeasurementDatasResponseDto { Data = data.Adapt<List<AggregatedResultDto>>() }
            );
        }
    }
}
