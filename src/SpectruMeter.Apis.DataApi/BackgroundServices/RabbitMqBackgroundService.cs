﻿using Mapster;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using SpectruMeter.Apis.DataApi.DAL.Models;
using SpectruMeter.Apis.DataApi.DAL.Repositories.Interfaces;
using SpectruMeter.Apis.DataApi.Models;
using System.Text;
using System.Text.Json;

namespace SpectruMeter.Apis.DataApi.BackgroundServices
{
    public class RabbitMqBackgroundService : BackgroundService
    {
        private readonly ILogger<RabbitMqBackgroundService> _logger;
        private readonly IConfiguration _configuration;
        private readonly IServiceProvider _sp;
        private ConnectionFactory _factory;
        private IConnection _connection;
        private IModel _measurementDataChannel;
        private IModel _householdChannel;
        private IModel _householdDeleteChannel;
        private IModel _householdUpdateChannel;
        private const string MEASUREMENT_DATA_EXCHANGE = "MeasurementDataExchange";
        private const string HOUSEHOLD_EXCHANGE = "HouseholdExchange";
        private string _measurementDataQueueName;
        private string _measurementDataDeleteQueueName;
        private string _householdDataQueueName;
        private string _householdUpdateQueueName;

        public RabbitMqBackgroundService(
            ILogger<RabbitMqBackgroundService> logger,
            IConfiguration configuration,
            IServiceProvider sp
        )
        {
            _logger = logger;
            _configuration = configuration;
            _sp = sp;
            _factory = new ConnectionFactory()
            {
                //AmqpUriSslProtocols = System.Security.Authentication.SslProtocols.Tls13,
                HostName = _configuration["RabbitMQ:Url"],
                //UserName = this._configuration["RabbitMQ:Username"],
                //Password = this._configuration["RabbitMQ:Password"],
            };

            _connection = _factory.CreateConnection();
            _measurementDataChannel = _connection.CreateModel();

            _measurementDataChannel.ExchangeDeclare(
                exchange: MEASUREMENT_DATA_EXCHANGE,
                type: "direct"
            );

            _measurementDataQueueName = _measurementDataChannel.QueueDeclare().QueueName;

            _measurementDataChannel.QueueBind(
                queue: _measurementDataQueueName,
                exchange: MEASUREMENT_DATA_EXCHANGE,
                routingKey: "measurement-data"
            );

            _householdChannel = _connection.CreateModel();

            _householdChannel.ExchangeDeclare(exchange: HOUSEHOLD_EXCHANGE, type: "direct");

            _householdDataQueueName = _householdChannel.QueueDeclare().QueueName;

            _householdChannel.QueueBind(
                queue: _householdDataQueueName,
                exchange: HOUSEHOLD_EXCHANGE,
                routingKey: "household-created"
            );

            _householdDeleteChannel = _connection.CreateModel();

            _householdDeleteChannel.ExchangeDeclare(
                exchange: MEASUREMENT_DATA_EXCHANGE,
                type: "direct"
            );

            _measurementDataDeleteQueueName = _householdDeleteChannel.QueueDeclare().QueueName;

            _householdDeleteChannel.QueueBind(
                queue: _measurementDataDeleteQueueName,
                exchange: MEASUREMENT_DATA_EXCHANGE,
                routingKey: "household-deleted"
            );

            //Update
            _householdUpdateChannel = _connection.CreateModel();

            _householdUpdateChannel.ExchangeDeclare(exchange: HOUSEHOLD_EXCHANGE, type: "direct");

            _householdUpdateQueueName = _householdUpdateChannel.QueueDeclare().QueueName;

            _householdUpdateChannel.QueueBind(
                queue: _householdUpdateQueueName,
                exchange: HOUSEHOLD_EXCHANGE,
                routingKey: "household-updated"
            );
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            if (stoppingToken.IsCancellationRequested)
            {
                _measurementDataChannel.Dispose();
                _connection.Dispose();
                return Task.CompletedTask;
            }

            var measurementDataConsumer = new EventingBasicConsumer(_measurementDataChannel);
            measurementDataConsumer.Received += MeasurementDataConsumer_Received;
            _measurementDataChannel.BasicConsume(
                queue: _measurementDataQueueName,
                autoAck: true,
                consumer: measurementDataConsumer
            );

            var householdConsumer = new EventingBasicConsumer(_householdChannel);
            householdConsumer.Received += HouseholdConsumer_Received;
            _householdChannel.BasicConsume(
                queue: _householdDataQueueName,
                autoAck: true,
                consumer: householdConsumer
            );

            var householdDeleteConsumer = new EventingBasicConsumer(_householdDeleteChannel);
            householdDeleteConsumer.Received += HouseholdDeleteConsumer_Received;
            _householdDeleteChannel.BasicConsume(
                queue: _measurementDataDeleteQueueName,
                autoAck: true,
                consumer: householdDeleteConsumer
            );

            var householdUpdateConsumer = new EventingBasicConsumer(_householdUpdateChannel);
            householdUpdateConsumer.Received += HouseholdUpdateConsumer_Received;
            ;
            _householdUpdateChannel.BasicConsume(
                queue: _householdUpdateQueueName,
                autoAck: true,
                consumer: householdUpdateConsumer
            );

            return Task.CompletedTask;
        }

        private void HouseholdUpdateConsumer_Received(object? sender, BasicDeliverEventArgs e)
        {
            _logger.LogInformation("Household update");
            var body = e.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            var data = JsonSerializer.Deserialize<HouseholdUpdateDto>(message);
            using var scope = _sp.CreateScope();

            _logger.LogInformation($"Household update {data}");
            var measurementDataRepository =
                scope.ServiceProvider.GetRequiredService<IMeasurementDataRepository>();

            measurementDataRepository.UpdateHouseholdAsync(data.HouseholdId, data.DeviceId).Wait();
        }

        private void HouseholdDeleteConsumer_Received(object? sender, BasicDeliverEventArgs e)
        {
            var body = e.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            var data = JsonSerializer.Deserialize<Guid>(message);
            using var scope = _sp.CreateScope();

            var measurementDataRepository =
                scope.ServiceProvider.GetRequiredService<IMeasurementDataRepository>();

            measurementDataRepository.DeleteAsync(data).Wait();
        }

        private void HouseholdConsumer_Received(object? sender, BasicDeliverEventArgs e)
        {
            _logger.LogInformation("Household create");

            var body = e.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            var data = JsonSerializer.Deserialize<HouseholdDto>(message);
            using var scope = _sp.CreateScope();

            _logger.LogInformation($"Household create: {data}");

            var measurementDataRepository =
                scope.ServiceProvider.GetRequiredService<IMeasurementDataRepository>();

            measurementDataRepository
                .InsertHousehouldDevice(
                    new HouseholdDevice
                    {
                        DeviceId = data.DeviceId,
                        HouseholdId = data.Id,
                        InsertedAt = DateTime.Now
                    }
                )
                .Wait();
        }

        private async void MeasurementDataConsumer_Received(object? sender, BasicDeliverEventArgs e)
        {
            try
            {
                var body = e.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                var data = JsonSerializer.Deserialize<MeasurementDataDto>(message);
                using var scope = _sp.CreateScope();

                var measurementDataRepository =
                    scope.ServiceProvider.GetRequiredService<IMeasurementDataRepository>();

                var householdDevice = await measurementDataRepository.GetHouseholdDeviceAsync(
                    data.DeviceId
                );

                var instertData = data.Adapt<MeasurementData>();
                instertData.HouseholdId = householdDevice?.HouseholdId ?? Guid.NewGuid();

                await measurementDataRepository.InsertAsync(instertData);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }
        }
    }
}
