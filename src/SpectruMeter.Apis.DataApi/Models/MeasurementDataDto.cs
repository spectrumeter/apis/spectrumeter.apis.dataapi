﻿using System.Text.Json.Serialization;

namespace SpectruMeter.Apis.DataApi.Models
{
    public class MeasurementDataDto
    {
        /// <summary>
        /// Device Id
        /// </summary>
        [JsonPropertyName("meternumber")]
        public string DeviceId { get; set; }

        /// <summary>
        /// Measurement Time
        /// </summary>
        [JsonPropertyName("timestamp")]
        public DateTimeOffset MeasurementTime { get; set; }

        /// <summary>
        /// Positive active instantaneous power (A+) [kW]
        /// </summary>
        [JsonPropertyName("active_power_plus")]
        public decimal PositiveActiveInstantaneousPower { get; set; }

        /// <summary>
        /// Positive active energy (A+) total [kWh]
        /// </summary>
        [JsonPropertyName("active_energy_plus")]
        public decimal PositiveActiveEnergy { get; set; }

        /// <summary>
        /// Negative active instantaneous power (A-) [kW]
        /// </summary>
        [JsonPropertyName("active_power_minus")]
        public decimal NegativeActiveInstantaneousPower { get; set; }

        /// <summary>
        /// Negative active energy (A+) total [kWh]
        /// </summary>
        [JsonPropertyName("active_energy_minus")]
        public decimal NegativeActiveEnergy { get; set; }

        /// <summary>
        /// Positive reactive energy (Q+) total [kvarh]
        /// </summary>
        [JsonPropertyName("reactive_power_plus")]
        public decimal PositiveReactiveEnergy { get; set; }

        /// <summary>
        /// Negative reactive energy (Q-) total [kvarh]
        /// </summary>
        [JsonPropertyName("reactive_power_minus")]
        public decimal NegativeReactiveEnergy { get; set; }

        /// <summary>
        /// Sum active instantaneous power(A+ - A-) [kW]
        /// </summary>
        public decimal SumActiveInstantaneousPower { get; set; }

        /// <summary>
        /// Instantaneous current(I) in phase L1[A]
        /// </summary>
        [JsonPropertyName("ampere1")]
        public decimal InstantaneousCurrentInPhaseL1 { get; set; }

        /// <summary>
        /// Instantaneous voltage (U) in phase L1 [V]
        /// </summary>
        [JsonPropertyName("voltage1")]
        public decimal InstantaneousVoltageInPhaseL1 { get; set; }

        /// <summary>
        /// Instantaneous current(I) in phase L2[A]
        /// </summary>
        [JsonPropertyName("ampere2")]
        public decimal InstantaneousCurrentInPhaseL2 { get; set; }

        /// <summary>
        /// Instantaneous voltage (U) in phase L2 [V]
        /// </summary>
        [JsonPropertyName("voltage2")]
        public decimal InstantaneousVoltageInPhaseL2 { get; set; }

        /// <summary>
        /// Instantaneous current(I) in phase L3[A]
        /// </summary>
        [JsonPropertyName("ampere3")]
        public decimal InstantaneousCurrentInPhaseL3 { get; set; }

        /// <summary>
        /// Instantaneous voltage (U) in phase L3 [V]
        /// </summary>
        [JsonPropertyName("voltage3")]
        public decimal InstantaneousVoltageInPhaseL3 { get; set; }
    }
}
