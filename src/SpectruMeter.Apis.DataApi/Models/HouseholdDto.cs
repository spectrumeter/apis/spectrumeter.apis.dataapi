﻿namespace SpectruMeter.Apis.DataApi.Models
{
    public class HouseholdDto
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public string? DeviceId { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string Number { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int Rooms { get; set; }
        public decimal SqaureMeters { get; set; }
    }
}
