﻿namespace SpectruMeter.Apis.DataApi.Models
{
    public class MeasurementDatasResponseDto
    {
        public List<AggregatedResultDto> Data { get; set; }
    }
}
