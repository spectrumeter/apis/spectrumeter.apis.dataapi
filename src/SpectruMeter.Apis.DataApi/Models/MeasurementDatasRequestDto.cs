﻿namespace SpectruMeter.Apis.DataApi.Models
{
    public class MeasurementDatasRequestDto
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public decimal Interval { get; set; }
        public string IntervalType { get; set; }
        public List<Guid> HouseholdIds { get; set; }
    }
}
